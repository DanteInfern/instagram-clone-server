const express = require('express')
const app = express()
const PORT = 5000
const mongoose = require('mongoose')
const {MONGOURI} = require('./keys')
//aSfdbKSqftYkQ4Vs

mongoose.connect(MONGOURI,{
    useNewUrlParser: true,
    useUnifiedTopology:true
})
mongoose.connection.on('connected',()=>{
    console.log("conectado");
})
mongoose.connection.on('error',()=>{
    console.log(" no conectado");
})

require("./models/user")
require("./models/post")
// mongoose.model("User")
app.use(express.json())
app.use(require('./routes/auth'))
app.use(require('./routes/post'))

app.listen(PORT,()=> {
    console.log("server is running ");
})
