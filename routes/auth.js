const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const User = mongoose.model("User")
const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../keys')
const requireLogin =  require('../middleware/requireLogin')


router.post('/signup',(req,res)=>{
    const {name,email,password} = req.body
    if (!email || !password || !name ){
        res.status(422).json({error:"please agrega todos los campos"})
    }
    User.findOne({email:email})
    .then((savedUser)=>{
        if(savedUser){
            return res.status(422).json({error:"user already exist wit that email"})
        }
        bcrypt.hash(password,12)
        .then(hashedPassword=>{
            const user = new User({
                email,
                password:hashedPassword,
                name
            })
            user.save().then(user=>{
                res.json({message:"saved succesfully"})
            }).catch(err=>{
                console.log(err);
            })
        })        
    })
})

router.post('/signin',(req,res)=>{
    const {email,password} = req.body
    if (!email || !password){
        return res.status(422).json({error:"please add email or password"})
    }
    User.findOne({email:email})
    .then(savedUser=>{
        if(!savedUser){
            return res.status(422).json({error:"Invalid email or password"})
        }
        bcrypt.compare(password,savedUser.password)
        .then(doMatch =>{
            if(doMatch){
                const token = jwt.sign({_id:savedUser._id},JWT_SECRET) 
                const {_id,name,email} = savedUser
                res.json({token,user:{_id,name,email}})
                // res.json({message:"succesfully singin"})
            }else{
                return res.status(422).json({error:"invalid email or password"})
            }
        })
    }).catch(err=>{
        console.log(err)
    })
})

module.exports =  router